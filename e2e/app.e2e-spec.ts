import { OTPSkyPage } from './app.po';

describe('otp-sky App', () => {
  let page: OTPSkyPage;

  beforeEach(() => {
    page = new OTPSkyPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

import {Component, OnInit} from "@angular/core";
import {MessageService} from "../message.service";
import {Router} from "@angular/router";
import {BitOperatorService} from "../bit-operator.service";
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css']
})
export class NewMessageComponent implements OnInit {

  msg = "Ihre Nachricht hier!";
  length = 0;
  private secret: Uint8Array;
  private fieldClicked: boolean = false;
  private min_size = 2048;

  color = 'primary';
  mode = 'determinate';
  value = 50;
  spinnerOn = false;

  //result: String;

  constructor(private MsgServ: MessageService, public snackBar: MdSnackBar, private bitop: BitOperatorService, private router: Router) {

  }

  updateCharCount() {
    this.length = this.msg.length;
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  deleteMsg() {
    if (!this.fieldClicked) {
      this.msg = "";
      this.fieldClicked = true;
    }
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var fr: FileReader = new FileReader();
    fr.readAsArrayBuffer(file);

    fr.onload = (data) => {
      this.secret = new Uint8Array(fr.result);
      this.secret = this.secret.slice(32, this.secret.length - 32);
      console.log("File read, length in bytes : " + this.secret.length);
    }

    //inputValue.files[0] = null;
    let snackBarRef = this.snackBar.open('Datei geladen.', "", {duration: 3000});
  }

  ngOnInit() {
    //   this.bitop.testBitwise();
  }

  sendMsg() {

    if (this.msg.length > this.secret.length) {
      console.log("Secret too short! Use bigger file.");
      //TODO popup alert
    } else {
      this.spinnerOn = true;
      console.log("Msg length in bytes : " + this.MsgServ.strToIa(this.msg).length);
      //this.msg = window.btoa(this.msg);
      let sec_length;
      if (this.msg.length * 2 <= this.min_size) {
        sec_length = this.min_size;
      } else {
        sec_length = this.msg.length * 2;
      }

      this.bitop.scrambleSecret(this.secret.slice(0, sec_length)).then(res => {
        this.spinnerOn = false;
        //  console.log("sendmsg as b64: " + window.atob(window.btoa(this.msg)));
        this.MsgServ.postMessage(this.msg, res).subscribe(
          data => {
            //console.log(data);
            this.router.navigate(['/create-link/' + data.hash]);
          },
          error => {
            console.log(error.json);
          });

      });


    }
  }


}

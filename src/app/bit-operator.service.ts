import {Injectable} from "@angular/core";

import {Md5} from "ts-md5/dist/md5";
//import {md5} from './md5';

@Injectable()
export class BitOperatorService {

  // private repeat = 10;

  constructor() {
  } //, private md5 : Md5) {


  getInt32Bytes(x) {
    var bytes = [];
    var i = 4;
    do {
      bytes[--i] = x & (255);
      x = x >> 8;
    } while (i)
    return bytes;
  }

  scrambleSecret(secret: Uint8Array): Promise<Uint8Array> {

    /**
     * The Core of this Application
     * Since the One-Time-Pad Method is unbreakable alone, this creates a unique Uint8Array for a File.
     * (Correct me if im wrong)
     */
    return new Promise((resolve, reject) => {

      console.log("Secret Length " + secret.length);

      /**
       * For Performance Measurement take the start time.
       * @type {Date}
       */
      var start = new Date();
      var s_milli = start.getTime();

      /**
       * The first element can't be 0 in this algorithm.
       * @type {boolean}
       */

      let cursor = 0;
      let offset = 0;

      // console.log(secret);

      console.log(secret.length ) /// 16);

      for (let k = 0; k < secret.length / 16; k++) {

        let slice = secret.slice(0 + offset, 16);
        offset += 16;
        let mdd = new Md5();
        mdd.appendByteArray(slice);
        let result = mdd.end(true);

        for (var i = 0; i < result.length; i++) {
          //console.log(result[i]);
          let int_array = this.getInt32Bytes(result[i]);

          for (var j = 0; j < int_array.length; j++) {
            //console.log(int_array[j]);
            secret[cursor] = int_array[j];
            cursor++;
          }
        }
      }



      //  console.log(secret);
      /**
       * Do this for half the size of the source array
       */

      for (var i = 1; i < secret.length / 2; i++) {

        /**
         * If current int is 0 we copy the last one and use XOR with the current i modulu 255 for a new value.
         */
        ////  secret[i] = secret[i - 1] ^ (i % 255);
        //console.log(i + " null. Set to " + secret[i]);
        //}

        /**
         * In Any Case XOR current int with the int on position x*2
         * @type {number}
         */

        secret[i] ^= secret[i * 2];

        /**
         * Shift current byte a variable amount (shift 1-3) and copy the result of "bit1 XOR bit4" to bit1.
         * Do this x - times.
         */

        let repeat = (secret[i] % 5) + 5;

        for (var shift = 1; shift <= repeat; shift++) {
          //  console.log("Shift = " + ((shift % 3) + 1));
          secret[i] = this.shiftWithoutLoss(secret[i], ((shift % 3) + 1));
          secret[i] = this.bitOneFourXor(secret[i]);
          //  console.log("Bin " + this.convertNumberToBits(secret[i]) + "/ Dez " + secret[i] + "/ Repeat " + repeat + " / Shift " + (shift % 3) + 1);
        }

      }


      /**
       * Measure End Time
       */

      var end = new Date();
      var e_milli = end.getTime();

     // console.log(secret);

      console.log("Time used for " + secret.length + " items\t\t" + (e_milli - s_milli) + " ms");

      /**
       * Cut Secret in Half, hence the 2nd half isnt scrambled yet.
       * @type {Uint8Array}
       */
      secret = secret.slice(0, secret.length / 2);
      // console.log("New Secret Length " + secret.length);

      resolve(secret);

    });
  }

  encrypt(x1: Uint8Array, x2: Uint8Array) {
    if (x1.length > x2.length) {
      console.log("Array1");
      console.log(x1);
      console.log("Array2");
      console.log(x2);
      console.error("encrypt failed, wrong sizes");
      return null;
    }

    for (var i = 0; i < x1.length; i++) {
      x1[i] ^= x2[i];
    }
    return x1;
  }


  shiftWithoutLoss(num: number, range: number) {
    // console.group("ShiftW/oLoss (range=" + range + ")");

    /**
     * Range cant be negative or 0, Values above 7 are pointless
     */
    if (range > 7 || range < 1) {
      console.log("Shift failed, wrong arguments");
      //console.groupEnd();
      return null;
    }
    // console.log("Input\t" + this.convertNumberToBits(num));
    /**
     * create mask the first "range" bits
     * @type {number}
     */

    let temp = 0xFF >> (8 - range);
    // console.log("Maske\t" + this.convertNumberToBits(temp));
    /**
     * Copy masked bits
     * @type {number}
     */
    temp &= num;
    // console.log("I & M: " + this.convertNumberToBits(temp));
    /**
     * Shift copied bits to destination
     * @type {number}
     */
    temp = temp << (8 - range);
    //  console.log("TempS: " + this.convertNumberToBits(temp));
    /**
     * Shift the result by range
     * @type {number}
     */
    num = num >>> range;
    //  console.log("NumSh: " + this.convertNumberToBits(num));
    /**
     * Use OR to bring back the lost bits
     * @type {number}
     */
    num |= temp;
    //  console.log("Result\t" + this.convertNumberToBits(num));
    //  console.groupEnd();
    return num;
  }

  bitOneFourXor(num: number) {
    //console.groupCollapsed("1XOR4");
    //  console.log("Input\t" + this.convertNumberToBits(num));
    /**
     * Set bitmask to 0001 0000 (we want the 4th bit, actually its the 5th)
     * @type {number}
     */
    let temp = 0x10;
    //  console.log("Mask\t" + this.convertNumberToBits(temp));
    /**
     * Use Mask to get the wanted bit
     * @type {number}
     */
    temp &= num;
    //  console.log("AND &\t" + this.convertNumberToBits(temp));
    /**
     * Shift the bit to position 1
     * @type {number}
     */
    temp = temp << 3;
    //  console.log("Shift\t" + this.convertNumberToBits(temp));
    /**
     * XOR the extracted bit with bit 1 and write result to bit 1
     * @type {number}
     */
    num ^= temp;
    //  console.log("Result\t" + this.convertNumberToBits(num));
    //  console.groupEnd();
    return num;

  }

  convertNumberToBits(num: number): String {
    /**
     * Number to Bits (String representation)
     * @type {string}
     */


    let temp = "";
    let i = 0;

    while (num > 0) {
      temp = (num % 2) + temp;
      num = num >> 1;
      i++;
    }

    /**
     * For better readability , fill up zeros
     */

    for (var j = i; j < 8; j++) {
      temp = "0" + temp;
    }

    return temp;
  }

  testBitwise() {

    /** IGNORE
     * FOR TESTING, will be deleted
     * @type {Date}
     */

    let array = new Uint8Array(512);
    let array2 = new Uint8Array(512);

    var start = new Date();
    var s_milli = start.getTime();

    this.scrambleSecret(array);

    var end = new Date();
    var e_milli = end.getTime();

    console.log("Time used for " + array.length + " items\t\t" + (e_milli - s_milli) + " ms");

  }

}

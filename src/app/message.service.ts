import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {BitOperatorService} from "./bit-operator.service";


@Injectable()
export class MessageService {

  private url = 'https://otp.skyit-webdesign.de/api/msg';
//  private url = 'http://localhost:3000/api/msg';

  constructor(private http: Http, private bitop: BitOperatorService) {
  }


  getMessage(hash: String): any {
    return this.http.get(this.url + "/" + hash)
      .map((res: Response) => res.json())
      .catch((err: any) => Observable.throw(err.json() || "Server Error"));
  }

  deleteMessage(hash : String ): any {
    return this.http.delete(this.url + "/" + hash)
      .map((res: Response) => res.json())
      .catch((err: any) => Observable.throw(err.json() || "Server Error"));
  }

  postMessage(msg: String, secret: Uint8Array): any {

    console.log(msg);
    let temp = this.bitop.encrypt(this.strToIa(msg), secret);
    console.log(temp);
    let request = {
      "msg": null
    }
    request.msg = this.iaToStr(temp);

    console.log("try post...");
    return this.http.post(this.url, request)
      .map((res: Response) => res.json())
      .catch((err: any) => Observable.throw(err.json() || "Server Error"))

  }

  iaToStr(array: Uint8Array) {
    let string = "";
    for (var i = 0; i < array.length; i++) {
      string += String.fromCharCode(array[i]);
    }
    return string;
  }

  strToIa(string: String): Uint8Array {
    let array = new Uint8Array(string.length);
    for (var i = 0; i < string.length; i++) {
      array[i] = string.charCodeAt(i);
    }
    return array;
  }




}

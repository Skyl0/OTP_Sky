import {Component, OnInit} from "@angular/core";
import {MessageService} from "../message.service";
import {ActivatedRoute} from "@angular/router";
import {BitOperatorService} from "../bit-operator.service";
import {MdDialog, MdDialogRef} from "@angular/material";



@Component({
  selector: 'app-receive-message',
  templateUrl: './receive-message.component.html',
  styleUrls: ['./receive-message.component.css']
})
export class ReceiveMessageComponent implements OnInit {

  hash = "";
  message: String = "";
  private secret: Uint8Array;
  private min_size: 2048;

  constructor(public dialog: MdDialog, private MsgServ: MessageService, private bitop: BitOperatorService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.hash = this.activatedRoute.snapshot.params['hash'];
  }

  openDialog() {
   // this.dialog.open(NotFoundDialog);
    alert("Nicht gefunden. :/");
  }

  readMsg() {

    this.MsgServ.getMessage(this.hash).subscribe(
      data => {
        //console.log( window.atob(data.msg));

        let sec_length;
        if (data.msg.length * 2 <= this.min_size) {
          sec_length = this.min_size;
        } else {
          sec_length = data.msg.length * 2;
        }

        this.bitop.scrambleSecret(this.secret.slice(0, sec_length )).then(res => {

          this.message = this.MsgServ.iaToStr(this.bitop.encrypt(this.MsgServ.strToIa(data.msg), res));

        });
        this.MsgServ.deleteMessage(this.hash).subscribe(
          data => {
            console.log("delete");
          },
          error => {
            console.log(error.json);
          });

      },
      error => {
        this.openDialog();
        console.log(error.json);
      });
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var fr: FileReader = new FileReader();
    fr.readAsArrayBuffer(file);

    fr.onload = (data) => {

      this.secret = new Uint8Array(fr.result);
      this.secret = this.secret.slice(32,this.secret.length-32);
    }

  }
}

@Component({
  selector: 'not-found-dialog',
  templateUrl: 'not-found-dialog.html',
})
export class NotFoundDialog {
}



import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'app-generate-link',
  templateUrl: './generate-link.component.html',
  styleUrls: ['./generate-link.component.css']
})
export class GenerateLinkComponent implements OnInit {
  link = 'https://otp.skyit-webdesign.de/receive/';
//link = "http://localhost:3000/receive/";
  hash = "";

  constructor(private activatedRoute: ActivatedRoute, public snackBar: MdSnackBar,) {
  }

  ngOnInit() {
    this.hash = this.activatedRoute.snapshot.params['hash'];
    this.link += this.hash;
  }

  copyOnClick() {
    window.prompt("Drücke Strg+C und dann Enter.", this.link);
    this.snackBar.open("Link kopiert! (hoffentlich)","",{ duration: 2000});
  }

}

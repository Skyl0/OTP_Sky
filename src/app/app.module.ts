import {HttpModule} from "@angular/http";
import {RouterModule, Routes} from "@angular/router";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {
  MdButtonModule,
  MdCardModule,
  MdCheckboxModule, MdDialogModule,
  MdIconModule,
  MdInputModule,
  MdMenuModule,
  MdProgressBarModule, MdProgressSpinnerModule,
  MdSnackBarModule
} from "@angular/material";
/**
 * Services
 */
import {MessageService} from "./message.service";
import {BitOperatorService} from "./bit-operator.service";
import {ProgressBarService} from "./progress-bar.service";
/**
 * Components
 */
import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {AppComponent} from "./app.component";
import {NewMessageComponent} from "./new-message/new-message.component";
import {ReceiveMessageComponent,  NotFoundDialog,} from "./receive-message/receive-message.component";
import {GenerateLinkComponent} from "./generate-link/generate-link.component";
import {LandingComponent} from "./landing/landing.component";
import {FaqComponent} from "./faq/faq.component";

/**
 * Routes
 */

const appRoutes: Routes = [
  {path: 'send', component: NewMessageComponent},
  {path: 'receive/:hash', component: ReceiveMessageComponent},
  {path: 'create-link/:hash', component: GenerateLinkComponent},
  {path: 'faq', component: FaqComponent},
  {path: '**', component: LandingComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    NewMessageComponent,
    ReceiveMessageComponent, NotFoundDialog,
    GenerateLinkComponent,
    LandingComponent,
    FaqComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdButtonModule, MdCheckboxModule, MdCardModule, MdInputModule,
    MdSnackBarModule, MdMenuModule, MdIconModule, MdProgressBarModule,
    MdProgressSpinnerModule, MdDialogModule,
    RouterModule.forRoot(appRoutes),

  ],
  providers: [
    MessageService,
    BitOperatorService,
    ProgressBarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

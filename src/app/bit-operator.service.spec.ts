import { TestBed, inject } from '@angular/core/testing';

import { BitOperatorService } from './bit-operator.service';

describe('BitOperatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BitOperatorService]
    });
  });

  it('should be created', inject([BitOperatorService], (service: BitOperatorService) => {
    expect(service).toBeTruthy();
  }));
});

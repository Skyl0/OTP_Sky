/**
 * Created by skyadmin on 15.06.2017.
 */
export class Message {
  constructor(
    public id: any,
    public msg: string,
    public hash: string
  ){}
}

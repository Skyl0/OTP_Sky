/**
 * Created by skyadmin on 14.06.2017.
 */
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const CryptoJS = require("crypto-js");
const https = require('https');
const fs = require('fs');

var options = {
  key: fs.readFileSync('/etc/letsencrypt/live/skyit-webdesign.de/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/skyit-webdesign.de/cert.pem'),
  ca: fs.readFileSync('/etc/letsencrypt/live/skyit-webdesign.de/chain.pem')
};

//var SHA256 = require("crypto-js/sha256");

const api = require('./server/routes/api');

// mongodb://root:1234@ds131041.mlab.com:31041/skyit_otp

//TODO put into dist
const secret = "hTWmuH8GXpBUPXhUkuVO";

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

/**
 * API
 */


/**
 * Create a Link to angular 2 dist folder
 */
//MongoClient.connect('mongodb://root:1234@ds131041.mlab.com:31041/skyit_otp', function (err, database) {
MongoClient.connect('mongodb://localhost:27017', function (err, database) {
  if (err) return console.log(err);
  db = database;

  //app.use('/api', api);

  app.use(express.static(path.join(__dirname, 'dist')));
  app.use("/static", express.static(path.join(__dirname, 'static')));

  app.get('/api/msg/:value', function (req, res) {
    console.log("[GET] Get Message with Hash " + req.params.value);

    db.collection("messages").findOne({hash: req.params.value}, function (err, result) {
      if (err) return console.log(err);
// TODO DELETE THAT entry after reading it
      res.send(result);
    });

  });

  app.post('/api/msg', function (req, res) {

    var salt = Math.random().toString(36);
    salt = salt.substr(salt.length - 14, 16);
    // console.log(salt);

    var hash = CryptoJS.AES.encrypt(salt, secret).toString();
    hash = hash.replace(/[^a-zA-Z0-9]/g, '');
    hash = hash.substr(hash.length - 17, 16);

    console.log('[POST] New Message with Hash ' + hash);

    var data = new Object();
    data.msg = req.body.msg;
    data.hash = hash;

    db.collection('messages').save(data, function (err, result) {
      if (err) return console.log(err);
      res.send(data);
    });

  });

  app.delete('/api/msg/:value', function (req, res) {
    var hash = req.params.value;
    db.collection('messages').remove( { hash: hash })
      /*, function (err, result) {
      if (err) return console.log(err);*/
    //});
  });


  /**
   * Port
   */

  const port = '3000';
  app.set('port', port);

  /**
   * Server Create
   * @type {"http".Server}
   */

  const server = http.createServer(app);
//  const server = https.createServer(options, app);
  /**
   * Standard Catch-All
   */

  app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });


  /**
   * Listen on provided port, on all network interfaces.
   */

  server.listen(port);
  console.log("Express Server started on Port " + port);
});
